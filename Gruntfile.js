module.exports = function(grunt) {

  grunt.initConfig({

    connect: {
      options: {
        port: 9000,
        livereload: 35729,
        hostname: '*' // * = accessible from anywhere ; default: localhost
      },
      livereload: {
        options: {
          open: false,
          base: [''] // '.tmp',
        }
      }
    },

    watch: {
      options: {
        livereload: '<%= connect.options.livereload %>'
      },
      html: {
        files: ['*.html']
      },
    }


  });

  // grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('server', ['connect', 'watch']);
  grunt.registerTask('default', ['server']);

};